# BigPaper

A collection of summaries from the mandatory readings of the [Big Data HS2020 lecture](http://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=140750&semkez=2020W&ansicht=KATALOGDATEN).

The compiled PDF can be found [here](./main.pdf)
