% ========== ========== ========== ==========
%
%  Bigtable: A Distributed Storage System for Structured Data
%
% ========== ========== ========== ==========
\section{Bigtable: A Distributed Storage System for Structured Data \small{- burgerm}}

\begin{quote}
    Source: \url{https://research.google.com/archive/bigtable-osdi06.pdf}
\end{quote}

Bigtable (in the following referred to as \emph{BT}) is a distributed storage system for managing \emph{structured data} and is designed to scale to very large sizes (PB of data).

It does not support a full relational data model, instead providing clients with a simple data model that supports dynamic control over data layout and format. \emph{BT} schema parameters let clients dynamically control whether to serve data out of memory or from disk, which allows to optimize latency-sensitive end user services or throughput-oriented batch processing.

\subsection{Data Model}

\begin{figure}[bh]
    \centering
    \includegraphics[width=\linewidth]{figures/bigtable_DataModel.png}
    \caption{Bigtable Data Model - Table Layout}
    \label{fig:bigtable-datamodel}
\end{figure}

A \emph{BT} is a sparse, distributed, persistent multi-dimensional map. The map is indexed (and sorted) by a row key, column key and a timestamp; each value in the map is an uninterpreted array of bytes. See Fig. \ref{fig:bigtable-datamodel} for the table layout.

\paragraph{Rows} The row keys are arbitrary strings and each operation under a given row key is atomic. A \emph{tablet} i.e. a range of row keys is the unit of distribution and load balancing. By choosing row keys which, in lexicographic order, group simultaneously accessed data together, performance can be optimized.

\paragraph{Column Families} Column keys are grouped into sets called \emph{column families} which form the basic unit of access control, as well as disk and memory accounting. 
All data stored in a column family is usually of the same type (data in same column family is compressed together.
Number of distinct column families in a table should be kept small and rarely change. But a table may have unlimited columns within these families and increase the column family size dynamically.

\paragraph{Timestamps} Each cell can contain multiple version indexed by timestamps and sorted in decreasing order for efficient access to the most recent element. Garbage collection keeps the number of versions in check based on preferences (keep $n$ most recent, keep 10 days back etc.).



\subsection{API}

Client app. can write or delete values, lookup indiv. rows or iterate over subset (range) of rows. Also supports single-transactions i.e. atomic read-modify write ops. under same row key. Client-supplied scripts can be run in the server address space to do inline data processing e.g. filter (but not write back). Tight integration with \emph{MapReduce} is supported.



\subsection{Structure}

\emph{BT} uses (distributed) GFS (Google File System) to store data. The Google \emph{SSTable} file format is used to persist data on disk. \emph{BT} relies on \emph{Chubby} a Paxos-based lock service, which handles failures, replication, access control etc.

One master server assigns \emph{tablets} to tablet servers, detects the addition and expiration of tablet servers, load balances the tablet servers and garbage-collects files from the underlying distributed file system (e.g. GFS); also handles schema changes. Client data does not go through the master; clients directly communicate with the tablet servers.

\begin{figure}[htb]
    \centering
    \includegraphics[width=.7\linewidth]{figures/bigtable_tabletLocation.png}
    \caption{Tablet location store hierarchy on the master server}
    \label{fig:bigtable-tabletlocation}
\end{figure}

The master server stores the tablet locations in a hierarchy similar to a B+ tree as can be seen in Fig. \ref{fig:bigtable-tabletlocation}.

\begin{figure}[htb]
    \centering
    \includegraphics[width=.6\linewidth]{figures/bigtable_storagePipeline.png}
    \caption{Tablet representation - persistent storage in GFS and fast memory storage in \textit{memtable}}
    \label{fig:bigtable-tablelayout}
\end{figure}

The tablet layout can be seen in Fig. \ref{fig:bigtable-tablelayout}. New data goes through a persistent commit log into a fast \textit{memtable}, which is then periodically flushed out to persistent storage in \emph{SSTable} files according to caching policies. Read requests are performed on a union of the \textit{memtable} and the \emph{SSTable} files. This process creates a new \emph{SSTabe} file upon each memtable flush. \emph{SSTabes} are periodically merged to keep the growth of the number of total \emph{SSTable} files logarithmically.



\subsection{Refinements}

\paragraph{Locality groups} allow clients to group often jointly accessed columns together and separate those that are usually not accessed together. This increases efficiency as unnecessary reads are prevented and writes have better locality.

\paragraph{Compression} can be configured for the SSTable file's blocks; the block level compression trades off some loss in space efficiency for faster access to partial pieces of a SSTable file as the blocks can be decompressed separately.

\paragraph{Caching} is performed in a \emph{Scan Cache} which caches key-value pairs of SSTables (good for temporal locality) and a \emph{Block Cache} caches SSTables read from GFS (good for spatial locality)

\paragraph{Bloom Filters} can improve access times as they store the potential candidate SSTable files and memtables, where a value could be stored.

\paragraph{Commit-log implementation} is tuned for performance and thus uses only one file for all tablets on a server; this however complicates recovery. Optimizations such as parallel sorting of the log file in case of recovery, two log files written by different threads to be able to switch to a different log file if performance is poor due to inconsistent GFS performance (log entries contain seq. numbers to order the two logs).