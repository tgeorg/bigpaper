% ========== ========== ========== ==========
%
%  HBase: The Definitive Guide (Chapters 1 and 3)
%
% ========== ========== ========== ==========
\section{HBase: The Definitive Guide (Chapters 1 and 3) \small{- jkleine}}

\begin{quote}
    Source: \url{http://proquest.safaribooksonline.com/9781449314682}
\end{quote}

HBase stores data on disk in a column oriented format. This column oriented format is basically the only similarity to RDBMS. While RDBMS allow real-time analytical access to data, HBase excels at providing key-based access to a specific cell of data, or a sequential range of cells.

HBase takes a lot of concepts from Bigtable. Rows are organized in columns which are grouped in column families. While there can only exist a few tens of column families, each family can contain millions of columns. The columns with a column family are stored together and can be expanded dynamically. They also reside in the same low level storage, i.e. one column family can be stored in faster storage than another.

While Null values must explicitly be written in a RDBMS, in HBase the value can simply be omitted.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5
    \textwidth]{figures/HBase/rows-cols.png}
    \caption{Rows and columns in HBase}
    \label{fig:hbase:row_col}
\end{figure}

Access to a single row is atomic, that is, any number of columns in that row can be read as written to atomically. However there are no atomic transactions spanning multiple rows.

\paragraph{Sharding} Contiguous rows are stored in \emph{regions}. These regions are stored on region servers. Each region is saved on exactly one \emph{region server}, with one region server is responsible for ideally 10-1000 regions, each region being 1GB-2GB in size. If a region becomes to large/small it is automatically split/fused to better balance the load between servers.

\paragraph{API} There exists a \emph{scan} API that allows to iterate over ranges of rows and be able to limit which columns are returned or the number of versions of each cell. Clients can also run client provided code in the address space of the server. This framework is called \emph{coprocessor}. The code has access to the server local data and can be used to implement lightweight batch jobs, or use expressions to analyze or summarize data based on a variety of operators. Finally, the system is integrated with the \emph{MapReduce} framework by supplying wrappers that convert tables into input source and output targets for MapReduce jobs.

\paragraph{Implementation} The data is stored in store files, called HFiles, which are persistent and ordered immutable maps from keys to values. The store files are typically saved in the Hadoop Distributed File System (HDFS), which provides a scalable, persistent, replicated storage layer for HBase. When data is updated it is first written to a commit log, called a write-ahead log (WAL) in HBase, and then stored in the in-memory memstore. Once the data in memory has exceeded a given maximum value, it is flushed as an HFile to disk. 

To assign regions to specific region servers the head server uses \emph{Apache ZooKeeper}

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{figures/HBase/implementation.png}
    \caption{HBase implementation build upon existing systems}
    \label{fig:hbase:implementation}
\end{figure}

\subsection{API Basics}

\paragraph{Put} The \texttt{put} method expects one or a list of \texttt{Put} objects. A put object is is created from a row identifier (a byte array \texttt{byte[] row}, though methods are provided to convert other data types into byte arrays, essentially allowing arbitrary key values), but can additionally take a row lock and/or a time stamp. Once created, data can be added to the put object through the use of add methods. The add method call specifies the family and qualifier of the column, together with the actual data (again given as byte array). Optionally one can pass another timestamp, if this is not passed the timestamp used in the constructor will be used instead. 

Alternative to specifying family, qualifier, and timestamp one may use the \texttt{KeyValue} object which identifies a cell in the three dimensional structure. \texttt{KeyValue}s offer more functionality than this, which is omitted here.

Each put operation is effectively a remote procedure call, which is not feasible if a client is storing thousands of values per second. For this purpose HBase provides a client-side write buffer. It automatically collects multiple stores by region and sends them collectively to the correct region.

HBase also provides a special put operation calles \texttt{checkAndPut} which allows for atomic, server-side mutations (comparable to CAS).

\paragraph{Get} Getting works very similar to setting. One specifies the row to create a \texttt{Get} object. This can then be narrowed down further by specifying a any one or more of the following: family, column, timestamp, and time range.

The call returns a \texttt{Result} object containing all the matching cells. Further details are omitted here.

\paragraph{Delete} The delete functionality again follows the same principle, by first having to create a \texttt{Delete} object by specifying the row, which can then be further narrowed down by column family, column, timestamp, etc..

Analogous to the \texttt{checkAndPut} a \texttt{checkAndDelete} also exists.

\paragraph{Row Locks} While normal operations like \texttt{put}, \texttt{delete}, and so on, are atomic, one might need more explicit control. If needed we can create a lock on a row in the first call and pass this lock to subsequent calls later on. Once not used anymore, the lock needs to be released again.

\paragraph{Scans} Scan is similar to get, but working on a range of rows. Like previously shown, we can narrow down the selection by specifying a column (family) and so on. Additionally a filter can be supplied to filter out unwanted rows in the range of rows.