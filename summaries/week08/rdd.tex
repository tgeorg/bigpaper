% ========== ========== ========== ==========
%
%  Resilient Distributed Datasets: A Fault-Tolerant Abstraction for In-Memory Cluster Computing
%
% ========== ========== ========== ==========
\section{Resilient Distributed Datasets: A Fault-Tolerant Abstraction for In-Memory Cluster Computing \small{- tgeorg}}
\label{paper:rdd}

\begin{quote}
    Source: \url{https://www.usenix.org/system/files/conference/nsdi12/nsdi12-final138.pdf}
\end{quote}

Resilient Distributed Datasets (RDDs) are "a distributed memory abstraction that lets programmers perform in-memory computations on large clusters in a fault-tolerant manner"


\subsection{Introduction}

Some cluster computing frameworks like MapReduce lack abstractions for leveraging distributed memory. There "the only way to reuse data between computations is to write it to an external stable storage system" which causes heavy I/O usage that can dominate application execution times.\\
As data reuse is common in some workloads such as iterative machine learning and graph algorithms a new framework is needed to provide efficient data reuse without causing heavy I/O while maintaining fault-tolerance. "The main challenge in designing RDDs is defining a programming interface that can provide fault tolerance efficiently."\\
Existing solutions provide consistency for fine-grained changes (e.g. updating cell in table). "In contrast to these systems, RDDs provide an interface based on coarse-grained transformations. [\dots] This allows them to efficiently provide fault tolerance by logging the transformations used to build a dataset (its lineage) rather than the actual data. If a partition of an RDD is lost, the RDD has enough information about how it was derived from other RDDs to recompute just that partition."\\
"RDDs are a good fit for many parallel applications, because these applications naturally apply the same operation to multiple data items."


\subsection{Resilient Distributed Datasets (RDDs)}
\paragraph{RDD Abstraction}
"An RDD is a read-only, partitioned collection of records. RDDs can only be created through deterministic operations on either data in stable storage or other RDDs." These operations are called \textit{transformations}.\\
"RDDs do not need to be materialized at all times. Instead, an RDD has enough information about how it was derived from other datasets (its lineage) to compute its partitions from data in stable storage."

\paragraph{Advantages of the RDD Model}
"The main difference between RDDs and distributed shared memory (DSM) is that RDDs can only be created (written) through coarse-grained transformations, while DSM allows reads and writes to each memory location. This restricts RDDs to applications that perform bulk writes, but allows for more efficient fault tolerance. [\dots] Furthermore, only the lost partitions of an RDD need to be recomputed upon failure, and they can be recomputed in parallel on different nodes, without having to roll back the whole program."\\
"A second benefit of RDDs is that their immutable nature lets a system mitigate slow nodes (stragglers) by running backup copies of slow tasks as in MapReduce."

Further, "in bulk operations on RDDs, a runtime can schedule tasks based on data locality to improve performance" and "RDDs degrade gracefully when there is not enough memory to store them, as long as they are only being used in scan-based operations. Partitions that do not fit in RAM can be stored on disk and will provide similar performance to current data-parallel systems."

\paragraph{Applications Not Suitable for RDDs}
Because RDDs perform updates on a coarse-grained level, it is not well suited for applications that make asynchronous fine- grained updates to shared state

\subsection{Spark Programming Interface}

"To use Spark, developers write a driver program that connects to a cluster of workers. The driver defines one or more RDDs and invokes actions on them." Spark code on the driver tracks RDDs' lineage. "Workers are long-lived processes that can store RDD partitions in RAM across operations."

\subsection{Representing RDDs}

The interface for RDDs exposes five pieces of information:
\begin{enumerate}
    \item \texttt{partitions()}: Return a list of Partition objects
    \item \texttt{preferredLocations(p)}: List nodes where partition $p$ can be accessed faster due to data locality
    \item \texttt{dependencies()}: Return a list of dependencies
    \item \texttt{iterator(p, parentIters)}: Compute the elements of partition $p$ given iterators for its parent partitions
    \item \texttt{partitioner()}: Return metadata specifying whether the RDD is hash/range partitioned
\end{enumerate}

Dependencies are split into \textit{narrow} and \textit{wide} dependencies.
\begin{itemize}
    \item \textbf{narrow:} "each partition of the parent RDD is used by at most one partition of the child RDD"
    \item \textbf{wide:} "where multiple child partitions may depend on it."
\end{itemize}
"This distinction is useful for two reasons. First, narrow dependencies allow for pipelined execution on one cluster node, which can compute all the parent partitions. For example, one can apply a map followed by a filter on an element-by-element basis. In contrast, wide dependencies require data from all parent partitions to be available and to be shuffled across the nodes using a MapReducelike operation. Second, recovery after a node failure is more efficient with a narrow dependency, as only the lost parent partitions need to be recomputed, and they can be recomputed in parallel on different nodes. In contrast, in a lineage graph with wide dependencies, a single failed node might cause the loss of some partition from all the ancestors of an RDD, requiring a complete re-execution."

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/rddDependencies.png}
    \caption{RDD Dependencies}
    \label{fig:rdd:dependencies}
\end{figure}

\subsection{Implementation}
\paragraph{Job Scheduling}


\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/rddDAG.png}
    \caption{Black boxes are RDD in RAM. B has already been computed and is in RAM hence Stage 2 is executed and then using the result (F) and B, G is produced in Stage 3}
    \label{fig:rdd:dag}
\end{figure}

"Whenever a user runs an action (e.g., count or save) on an RDD, the scheduler examines that RDD’s lineage graph to build a DAG of stages to execute."\\
"If a task needs to process a partition that is available in memory on a node, we send it to that node. Otherwise, if a task processes a partition for which the containing RDD provides preferred locations (e.g., an HDFS file), we send it to those.[\dots] If a task fails, we re-run it on another node as long as its stage’s parents are still available. If some stages have become unavailable (e.g., because an output from the “map side” of a shuffle was lost), we resubmit tasks to compute the missing partitions in parallel."

\paragraph{Memory Management}
"Spark provides three options for storage of persistent RDDs: in-memory storage as deserialized Java objects, in-memory storage as serialized data, and on-disk storage."\\
"The first option provides the fastest performance, because the Java VM can access each RDD element natively. The second option lets users choose a more memory-efficient representation than Java object graphs when space is limited, at the cost of lower performance. 8 The third option is useful for RDDs that are too large to keep in RAM but costly to recompute on each use." To manage limited memory, an LRU eviction policy at the level of RDDs is used.

\paragraph{Support for Checkpointing}
"Although lineage can always be used to recover RDDs after a failure, such recovery may be time-consuming for RDDs with long lineage chains. Thus, it can be helpful to checkpoint some RDDs to stable storage."

\subsection{Discussion}
\paragraph{Expressing Existing Programming Models}
RDDs can efficiently express a various cluster programming models like MapReduce and performing distributed SQL queries.

\paragraph{Leveraging RDDs for Debugging}
For fault tolerance, RDDs are designed to be deterministically recomputable which allows for easy replaying of computations. This property also facilitates debugging.

\subsection{Conclusion}

Resilient distributed datasets (RDDs) "can express a wide range of parallel applications, including many specialized programming models that have been proposed for iterative computation, and new applications that these models do not capture. Unlike existing storage abstractions for clusters, which require data replication for fault tolerance, RDDs offer an API based on coarse-grained transformations that lets them recover data efficiently using lineage."
