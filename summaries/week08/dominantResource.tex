% ========== ========== ========== ==========
%
%  Dominant Resource Fairness: Fair Allocation of Multiple Resource Types
%
% ========== ========== ========== ==========
\section{Dominant Resource Fairness: Fair Allocation of Multiple Resource Types \small{- jkleine}}

\begin{quote}
    Source: \url{https://www.usenix.org/legacy/events/nsdi11/tech/full_papers/Ghodsi.pdf}
\end{quote}

The intuition behind Dominant Recourse Fairness (DRF) is that in a multi-resource environment, the allocation of a user should be determined by the user’s dominant share, which is the maximum share that the user has been allocated of any resource. In a nutshell, DRF seeks to maximize the minimum dominant share across all users.

\paragraph{Principal} Manage a job by its most dominant resource. For example if a job requires 10\% of the available CPU and 30\% of the available Memory, treat it like the job requested 30\% of the cluster. Then try to maximize the overall allocation of the system, while trying to match the dominant shares the the theoretical shares of the system, and making sure the total doesn't exceed any one recourse (i.e. if 100 GB are available, don't allocate 120 GB...). One can formulate this as LP but I couldn't be bothered.

\paragraph{Example} Assume the resources (9 cores, 18 GB memory) are supposed to be split 50/50, user A runs jobs that need 1 CPU and 4GB memory each, while user B runs jobs that require 3 CPUs and 1 GB memory each. The dominant recourse utilization are \(4/18 = 2/9\) for user A and \(3/9 = 1/3\) for user B. We now try to allocate as many tasks as possible, while balancing the dominant recourse as given by the 50/50 split. The result is that user A can run 3 jobs and user B can run 2. This way the dominant resources are distributed as follows: A: \(3 \cdot 2/9 = 2/3\), and B: \(2 \cdot 1/3 = 2/3\).

Note that even though both users seem to be allocated two thirds of the system, this is not the case. The goal is just to keep these number in relation to the reservation of the system (i.e. 50/50, so equal in this case). 

The actual utilization of the system is thus \(3 \cdot 1 + 2 \cdot 3 = 9\) CPUs, and \(3 \cdot 4 + 2 \cdot 1 = 14\) GB memory.

\paragraph{Properties} The following properties should be fulfilled by a resource manager:

\begin{itemize}
    \item \emph{Sharing incentive:}  Each user should be better off sharing the cluster, than exclusively using her own partition of the cluster. Consider a cluster with identical nodes and n users. Then a user should not be able to allocate more tasks in a cluster partition consisting of n1 of all resources.
    
    \item \emph{Strategy-proofness:} Users should not be able to benefit by lying about their resource demands. This provides incentive compatibility, as a user cannot improve her allocation by lying.
    
    \item \emph{Envy-freeness:} A user should not prefer the allocation of another user. This property embodies the notion of fairness [13, 30].
    
    \item \emph{Pareto efficiency:} It should not be possible to increase the allocation of a user without decreasing the allocation of at least another user. This property is important as it leads to maximizing system utilization subject to satisfying the other properties.
    
    \item \emph{Single resource fairness:} For a single resource, the solution should reduce to max-min fairness.
    
    \item \emph{Bottleneck fairness:} If there is one resource that is percent-wise demanded most of by every user, then the solution should reduce to max-min fairness for that resource.

    \item \emph{Population monotonicity:} When a user leaves the system and relinquishes her resources, none of the allocations of the remaining users should decrease.

    \item \emph{Resource monotonicity:} If more resources are added to the system, none of the allocations of the existing users should decrease.
\end{itemize}

Dominant Recourse fairness achieves all but the last property, which is also not satisfied by the systems they use for comparison.

