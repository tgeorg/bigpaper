% ========== ========== ========== ==========
%
%  Spark SQL: Relational Data Processing in Spark
%
% ========== ========== ========== ==========
\section{Spark SQL: Relational Data Processing in Spark \small{- burgerm}}

\begin{quote}
    Source: \url{https://doi.org/10.1145/2723372.2742797}
\end{quote}

\emph{Spark SQL} is a module in Apache Spark that integrates relational processing with Spark's functional programming API. The goal is to provide users with an interface that seamlessly mixes relational (declarative database style) and procedural programming models to access data and perform computations.
This is realized through the \emph{Spark DataFrame API} and a new optimizer \emph{Catalyst}. The base Spark API is a general-purpose cluster copmuting engine with APIs in Scala, Java, and Python and libraries for streaming, graph processing and machine learning.

It is based on a functional programming API, where users manipulate RDDs. Please refer to paper summary section \ref{paper:rdd} for more information on RDDs and basic Spark operation.


\paragraph{Goals for Spark SQL}

\begin{itemize}
    \item Support relational processing within Spark on native RDDs and on external data sources
    \item Provide high performance using established DBMS techniques/optimizations
    \item Easily support new data sources; including semi-structured and external databases
    \item Enable extensions for graph processing and machine learning
\end{itemize}


\subsection{Programming Interface}

\paragraph{DataFrame API} is the main abstraction in Spark SQL; a distributed collection of rows with the same schema. It is equivalent to a table in relational databases. They keep track of their schema and support relational operations, thus leading to more optimized execution.

\emph{DataFrames} can be created from external sources or RDDs and then be manipulated by relational operations or also viewed as an RDD of \emph{Row objects}, which can be manipulated by the procedural Spark API functions.

All operations are \emph{lazy} and are only executed upon a specific request to compute the results; this allows for rich set of optimizations.

\paragraph{Data Model} Spark SQL supports all the known base types (booleans, integers etc.), complex data types (structs, arrays, maps etc.), as well as custom complex data types.

\paragraph{DataFrame Operations} are expressed in a DSL (domain specific language), which includes all common relational operations such as projection (\texttt{select}), filter (\texttt{where}), \texttt{join}, and aggregations (\texttt{groupBy}). The operators take \emph{expressions} as arguments in a limited DSL, which allows Spark to capture the structure of the computation in an AST (abstract syntax tree) which is passed to the optimizer \emph{Catalyst}. This is opposed to the native Spark API which executes arbitrary host framework code, which is opaque to the runtime engine.

The DataFrames can also be registered as temporary tables and queried using SQL. To simplify programming in DataFrames the API can \emph{analyze} logical plans \emph{eagerly} to identify errors, but still \emph{compute lazily}.

Spark can also infer schemas from language native collections of objects. For example inferring a schema from an RDD of class objects by directly using class attribute names and create the appropriate DataFrame.

\paragraph{In-Memory Caching} is more efficient as the compact columnar storage of DataFrames is more space-efficient than the object-based storage of RDDs.

\paragraph{UDF (User-Defined Functions} can be registered in Spark SQL inline by passing native language (Scala, Python, Java) functions. This allows to extend the framework for application specific processing in e.g. machine learning.


\subsection{Catalyst Optimizer} 

\emph{Catalyst} is an extensible optimizer based on functional programming constructs in Scala. It is at its core based on a general library for representing \emph{trees} and \emph{rules} to manipulate them. The main datatype in Catalyst is thus a \emph{tree} with \emph{node} objects of various types. Trees can be manipulated by rules, which define a tree-to-tree mapping; rules are applied using \emph{pattern-matching} (built into many functional programming languages; thus also Scala) on sub-trees. Rules can be applied until it reaches a \emph{fixed point} i.e. the tree does not change anymore.

\paragraph{Catalyst in Spark SQL} and its tree-based approach is used for:

\begin{itemize}
    \item Analyzing a logical plan to resolve references and types.
    \item logical plan optimization (based on common DBMS techniques)
    \item physical planning using Spark physical operators (multiple results may be compared based on cost)
    \item code generation to compile parts of the query to Java bytecode
\end{itemize}

\paragraph{Extension points} Users can extend Spark SQL through adding:

\begin{itemize}
    \item Custom \emph{Data Sources} by using several APIs implementing various levels of access to the data source from simply scanning it completely to use e.g. native filtering implementations of the data source.
    \item User-Defined Types (UDT) (e.g. vector type for ML, graph types for graph processing etc.); UDTs are mapped to structures of built-in types using Catalyst (the user provides the mapping upon implementing the UDT). This allows the framework to use all the optimizations also on the UDTs.
\end{itemize}


\subsection{Advanced Analytics Features}

Features added to improve \emph{big data} type of workloads.

\begin{itemize}
    \item Schema Inference for semi-structured data e.g. JSON input data
    \item Integration with Sparks ML library by exposing an API based on DataFrames; allowing to build ML \emph{pipelines}
    \item Query Federation to External Databases; instead of loading data into local processing environment, execute the required query or parts of it on the external data source directly to reduce traffic and improve performance.
\end{itemize}


\subsection{Evaluation, Research Applications, Related Work}

DataFrames make everything faster, simpler, and even researchers like Spark SQL.

