% ========== ========== ========== ==========
%
%  Amazon Dynamo Key-Value Store
%
% ========== ========== ========== ==========
\section{Dynamo: Amazon Key-Value store \small{- jkleine}}

\begin{quote}
    Source: \url{https://dl.acm.org/doi/10.1145/1323293.1294281}
\end{quote}

Dynamo is a highly available key-value store used internally by Amazon and backs many of their services. It follows a simple query model, allowing only simple \emph{put} and \emph{get} instructions to store \emph{blobs} (binary objects) usually less than 1 MB in size. No operations span multiple data items and there is no need for relational schemes. Dynamo sacrifices consistency to some extend to provide high availability. Note Dynamo is designed for Amazon internal use only and assumes the operating environment to be non-hostile, thus authentication and authorization are not part of the design considerations.

The goal is to have a system that conforms to latency constrains in the 99.9\textsuperscript{th} percentile of of the distribution. Additionally Dynamo targets the design space of an ``always writable'' data store, meaning write requests are not rejected even during failure of nodes and network partitions. Additionally the system should embrace the following principles:
\begin{itemize}
    \item \emph{Incremental scalability}: Dynamo should be able to scale out one storage node at a time, with minimal impact on both operators of the system and the system itself.
    
    \item \emph{Symmetry}: Every node in Dynamo should have the same set of responsibilities as its peers; there should be no distinguished node or nodes that take special roles or extra set of responsibilities.

    \item \emph{Decentralization}: An extension of symmetry, the design should favor decentralized peer-to-peer techniques over centralized control.

    \item \emph{Heterogeneity}: The system needs to be able to exploit heterogeneity in the infrastructure it runs on. e.g. the work distribution must be proportional to the capabilities of the individual servers. This is essential in adding new nodes with higher capacity without having to upgrade all hosts at once.
\end{itemize}

\subsection{System Architecture}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/dynamo_partitoning_strategies.png}
    \caption{Overview of strategies explored to divide the hash space. Strategy 1 nodes are assigned a random token and store all the data between it and the N\textsuperscript{th} node in counter-clockwise direction. Strategy 2 was deemed inefficient, not further discussed in the summary (mix of 1 \& 3). Strategy 3 divides the space into $Q$ equally sized sections, assigning each node $Q/S$ sections. each section is replicated by the first $N$ distinct nodes in clockwise direction.}
    \label{fig:dynamo:strategy}
\end{figure}

\paragraph{Partitioning} Dynamo partitioning relies on consistent hashing (in the form of MD5 hash of the key) to extract a 128-bit identifier that maps the object to circular space. This circular space is divided among the storage nodes. To account for heterogeneous hardware, nodes in the system are implemented as virtual nodes with one physical node running multiple virtual nodes depending on its hardware resources. Each virtual node is assigned a position on the ring (called \emph{token}). This virtual node is then the coordinator of all the keys between its token and the token of the predecessor (next virtual node counter clockwise on the ring).

The specific strategy by which is the circle is devided between the nodes is visualized in Figure \ref{fig:dynamo:strategy} Strategy 3. The space is divided into $Q$ equal parts (tokens), with ever node being assigned $Q/S$ tokens (where $S$ is the number of nodes in the system and $Q >> S$). Additionally each token is also replicated by the first $N$ distinct nodes in clockwise direction on the circle. When a node leaves the network, its tokens are divided among the remaining nodes, such that every node again has $Q/S$ tokens. Analogously, when a node enters the network it steals an equal amount of tokens from the other nodes.

The nodes replicating a particular key are stored in a \emph{preference list}.

\paragraph{Execution of \texttt{get()} and \texttt{put()}} The node handling the read or write operation is called the \emph{coordinator}. Typically its the first node in the preference list, but load balancing might decide to give the task to a different node on the preference list.

To maintain consistency the nodes follow a protocol that mimics quorums, called \emph{sloppy quorums}. Hereby every read request needs to be answered by $R$ nodes, and every write request needs to be answered by $W$ nodes. More specifically the coordinator will generate a vector clock for the new item and writes it locally. It then sends the new item along with the vector clock to all the $N$ highest ranked reachable nodes. If $W$ nodes respond the write operation is considered successful. Choosing $R + W > N$ guarantees that the set of nodes that answered the write requests and the nodes answering the read request overlap. Alternatively choosing $W = 1$ guarantees that write requests will be answered as long as there is one active node left in the system. This allows to configure different Dynamo instances to the specific use case.

\paragraph{Handling Failures} To handle temporary node failures, Dynamo implements \emph{hinted handoffs}, where messages send to a node that is down will be forwarded to the next available node. This node will detect that the message was intended for another node and will save it in a separate local database. Upon reconnecting with the failed node it will send all the data from the local database to the failed node to bring it ``up to date``.

For permanent node failure, where this hinted handoff is not enough to recover the data (the node temporarily storing the data might fail as well), Dynamo synchronizes replica nodes regularly. To minimize the transferred data Dynamo uses Merkel trees, where each leaf is the hash of some data and the inner nodes are the hash of the child nodes. This way it is possible to find where exactly the data differs without sending unnecessary copies.