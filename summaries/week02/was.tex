% ========== ========== ========== ==========
%
%  Windows Azure Storage
%
% ========== ========== ========== ==========
\section{Windows Azure Storage (WAS) \small{- burgerm}}

\begin{quote}
    Source: \url{https://sigops.org/s/conferences/sosp/2011/current/2011-Cascais/printable/11-calder.pdf}
\end{quote}

To store seemingly limitless amounts of data for any duration of time and pay only what is being used. WAS provides cloud storage in the form of \emph{Blobs} (user files), \emph{Tables} (structured storage) and \emph{Queues} (message delivery). WAS claims to satisfy \emph{strong consistency}, \emph{high availability} and \emph{partition tolerance} all at the same time; ie. all of \emph{CAP}.

\subsection{Global Partitioned Namespace}

WAS leverages DNS to provide a single global and scalable namespace. The storage namespace is broken down into three parts:

\begin{itemize}
    \item \emph{account name} is the customer selected account to access the data; used to locate primary storage cluster for data, all requests to access this account's data go to said cluster
    \item \emph{partition name} locates data within cluster, used to scale out data access across nodes if required by traffic needs
    \item \emph{object name} used to access indiv. objects of a partition if present. A partition name can also directly point to data itself depending on the type of data. Atomic operations are supported across objects.
\end{itemize}

Access data by \url{http(s)://[AccountName].<service>.core.windows.net/[PartitionName]/[ObjectName]}

\begin{itemize}
    \item Blob: access by Partition Name
    \item Table: each row has primary key (partitionName, objectName), can group rows into same partition to make Table
    \item Queues have Partition Name with messages having an Object Name
\end{itemize}

\subsection{High Level Architecture}

WAS is part of the Windows Azure Cloud platform. The Windows Azure Fabric Controller provides node management, network configuration, health monitoring, starting/stopping of service instances and service deployment. WAS is responsible for replication, placement, load balancing. 

\paragraph{Storage Stamp} is a cluster of $N$ racks of storage nodes, each rack being an independent fault domain with redundant networking and power. To optimize economically a stamp should be kept at about $70\%$ utilization in terms of capacity, transactions and bandwidth, but avoid going over $80\%$ to maintain high performance and lifetime. Inter-stamp replication manages migration to maintain good utilization levels.

\paragraph{Location Service (LS)} manages all the storage stamps and the account namespace and assignment of accounts across the stamps. Itself distributed and redundant, performs disaster recovery and load balancing by updating DNS entries to the respective exposed VIPs (Virtual IP) of the assigned stamp.

\paragraph{Three Stamp Layers} bottom-up
\begin{itemize}
    \item Stream Layer: stores data on disks and responsible for durability i.e. replication across the stamp (sort of a distr. file system within a stamp. \emph{Files} called \emph{Streams}, which are ordered lists of \emph{Extents}.
    \item Partition Layer: manage, address, caching, consistency of the data and provide higher level abstractions (Blob, etc.). Scalability and load bal.
    \item Front-End (FE) Layer: Set of stateless servers responsible for request handling, authentication and authorization, also cache freq. accessed data directly
\end{itemize}

\paragraph{Two replication engines}
\begin{itemize}
    \item Intra-Stamp Replication \emph{synchronous} in the Stream Layer; replicate blocks of disk storage, fault-tolerance
    \item Inter-Stamp Replication \emph{asynchronous} in the Partition Layer; replicate objects and related transactions, geo-redundancy
\end{itemize}

\subsection{Stream Layer}

Provides internal interface used by Partition Layer only, see Fig. \ref{fig:azureStorageStream} for the layout. Write operations are append-only and appends are atomic (entire block or nothing), supports atomic multi-block append. Only last \emph{Extent} can be appended to, others are \emph{sealed} (sealed extents are immutable). Blocks are the minimum unit of storage and checksum validation is performed on a block-level for data integrity. Extents are the unit of replication in the stream layer. Depending on object size several extents are used or several objects put into even the same block. Stream looks like a big file to the Partition Layer. Partition Layer handles timeouts, retries and duplicates.

\begin{figure}[htb]
    \centering
    \includegraphics[width=\linewidth]{figures/azureStorageStream.png}
    \caption{Stream Layout with Extents and Blocks}
    \label{fig:azureStorageStream}
\end{figure}

The \emph{Stream Manager (SM)} manages a set of \emph{Extent Nodes (EN)} and forms a Paxos-based cluster. The SM is only aware of Streams and Extents, but not Blocks. Each EN maintains storage for a set of extent replicas assigned to by the SM. An overview is in Fig. \ref{fig:azureStorageStreamManager}. The primary EN targeted by client is in charge of coordinating the writes to the secondary ENs.

\begin{figure}[htb]
    \centering
    \includegraphics[width=0.7\linewidth]{figures/azureStorageStreamManger.png}
    \caption{Stream Manager Overview}
    \label{fig:azureStorageStreamManager}
\end{figure}

The SM coordinates the sealing operation among the ENs. Unavailable ENs during sealing will later be forced to sync to sealed commit length. The partition layer can read at specific locations (extend + location, length) and upon partition loading the metadata and commit log (both in streams) are being read sequentially until the end.

To save space sealed extents are erasure coded and fragments are distributed, which replaces the three-fold replication for sealed extents. If an extent cannot respond fast enough to a request, the request cancelled and can be redirected to a different EN by the client.

Spindle disks are optimized for long sequential operations, to still ensure fairness to e.g. short random access custom IO scheduling is used at the cost of slight increase in latency.

For durability a write is only acknowledged to the client after three copies have been made to durable storage. A \emph{journal drive} (e.g. SSD) is being fed sequentially with incoming writes to the EN to saturate bandwidth and the write is ack'ed as soon as the journal has been written or the actual destination. 

\subsection{Partition Layer}

The partition layer (Overview in Fig. \ref{fig:was_partitionLayer} stores the different types of objects and understands what a transaction for a specific object means (Blob, Table, Queue). The partition layer provides the \emph{Object Table} (OT). OTs (up to Petabytes large) are broken into \emph{RangePartitions}. A \emph{RangePartition} is a contiguous range of rows from a given low-key to a high-key. OTs are Account Table, Blob Table, Entity Table, Message Table, Schema Table, Partition Map Table. They support standart insert, update, delete ops. on rows as well as query/get ops.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{figures/azureStoragePartitionManager.png}
    \caption{Partition Layer Overview, the \emph{Lock Service} (Paxos based) determines the current leader PM and ensures the PS have non-overlapping partition ranges}
    \label{fig:was_partitionLayer}
\end{figure}

The \emph{Partition Manager} (PM) splits the OTs into RangePartitions and assigns them to Partition Servers (PS), which serve the requests for assigned partitions. Load balancing takes place by reassigning RangePartitions to PS and splitting/merging RangePartitions.

Each \emph{RangePartition} holds streams in the Stream Layer for Metadata, Commit-Log, Row-Data and Blob-Data. Various data-structures are kept in-memory for faster access and caching. The PS uses a commit log, checkpointing and in-memory buffering to achieve both durability and high throughput.

The Partition Layer in the primary stamp will asnychronously geo-replicate changes to the secondary stamp using inter-stamp replication.

\subsection{Further sections}

\paragraph{Application Throughput} lays out some performance analysis: scaling number of worker VMs, table throughput, blob throughput etc.

\paragraph{Workload Profiles} shows various large-scale example workloads for using WAS.

\paragraph{Design choices}

\begin{itemize}
    \item separate computation from storage for cloud offerings; allows to scale the two independently.
    \item range-based partitioning/indexing instead of hash-based (recall e.g. the assignment of the RangePartitions to the PS), hashes would auto-load balance, but ranges keep related data close (e.g. same customer data etc.)
    \item Others: Throttling/Isolation (of e.g. accounts), Auto. Load-Balance (of partitions), Append-only system, Upgrades (Fault and upgrade domains)
    \item CAP, claimed to hold within a single storage stamp under conditions observed in practice i.e. within their fault model.
\end{itemize}


