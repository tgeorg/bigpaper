% ========== ========== ========== ==========
%
%  Robinson, I. et al. (2015). Graph Databases (2nd ed.)
%
% ========== ========== ========== ==========
\section{Robinson, I. et al. (2015). Graph Databases (2nd ed.) \small{- jkleine}}

\begin{quote}
    Source: \url{http://proquest.safaribooksonline.com/book/databases/9781491930885}
\end{quote}

\subsection{Data Modeling with Graphs}

\subsubsection{Labeled Property Graph}

A labeled property graph is made up of \textit{nodes}, \textit{relationships}, \textit{properties}, and \textit{labels}. Nodes contain properties and represent documents that store properties in the key-value format. In Node4j valid values are (Java) Strings, primitive data types and arrays of the previous types. Nodes are tagged with labels to group nodes together (for example each node represents a Person, then Person would be the tag). A relationships connect two nodes to from a graph. The connections are directed and never dangling.

\subsubsection{Querying Graphs with Cypher}

The philosophy of Cypher is to ``find things like this'' in the Graph. ``Things like this'' are graph patterns, intuitively defined by ASCII art. For example, the ASCII art
\begin{code}
\begin{asciiListing}
    (emil)<-[:KNOWS]-(jim)-[:KNOWS]->(ian)-[:KNOWS]->(emil)
\end{asciiListing}
\end{code}
defines the graph in Firgure \ref{fig:graphdb:sample_pattern}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{figures/week12/sample_pattern.png}
    \caption{Sample graph pattern}
    \label{fig:graphdb:sample_pattern}
\end{figure}

To further specify what we are looking fore, we can define a few more properties, like node type, properties, etc.. The new ASCII art looks like follows:

\begin{code}
\begin{asciiListing}
    (emil:Person {name:'Emil'})
      <-[:KNOWS]-(jim:Person {name:'Jim'})
      -[:KNOWS]->(ian:Person {name:'Ian'})
      -[:KNOWS]->(emil)
\end{asciiListing}
\end{code}

\paragraph{\texttt{\bf MATCH}} The match clause is the specification by example part of the query language. The query selects all matches of the patter. Combining this with the \texttt{RETURN} clause we can specify what part of the matched patter to return. We can also define further properties using the \texttt{WHERE} clause, very similar to how SQL works. Take a look at the following query:

\begin{code}
\begin{asciiListing}
    MATCH (a:Person)-[:KNOWS]->(b)-[:KNOWS]->(c), (a)-[:KNOWS]->(c)
    WHERE a.name = 'Jim'
    RETURN b, c
\end{asciiListing}
\end{code}

This query would match all triangle patterns (similar to Figure \ref{fig:graphdb:sample_pattern}), where a node \textit{a} ``knows'' a node \textit{b}, which in turn ``knows`` \textit{c}, \textit{a} also knows \textit{b} directly. Further \textit{a} is labeled as Person. The \texttt{WHERE} clause further specifies that node \textit{a} must have a property \textit{name} with value \textit{``Jim''}. The query returns the nodes \textit{b} and \textit{c} of all matching occurrences.

A query can return node, relationships, and properties from the matched data.

The nodes in a query that map to specific real nodes are called \textit{anchors} as they anchor the query to real nodes.

Further syntax examples:
\begin{code}
\begin{asciiListing}
    1.  (a:Label {prop:'value'})-[r:RELATION]->(b:Label)
    2.  (a)-[]->()
    3.  (a)-[:REL_A|REL_B*1..3]->(b)
\end{asciiListing}
\end{code}
The first is pretty basic, it matches a node with label ``Label'' and property ``prop'' equal to \texttt{'value'}, which is connected via a relation ``RELATION'' to another node with label ``Label''. The nodes are identified by \textit{a} and \textit{b} respectively, the relationship by \textit{r}. These identifiers can be used in the rest of the query, for example in \texttt{RETURN} and \texttt{WHERE} clauses. If the node is not needed specifically one can use the \textit{anonymous} node

\paragraph{List of other Cypher clauses}

\begin{itemize}
    \item[\texttt{WHERE}] Provides criteria for filtering pattern matching results.
    \item[\texttt{CREATE}] (and \texttt{CREATE UNIQUE}) Create nodes and relationships.
    \item[\texttt{MERGE}] Ensures that the supplied pattern exists in the graph, either by reusing existing nodes and relationships that match the supplied predicates, or by creating new nodes and relationships.
    \item[\texttt{DELETE}] Removes nodes, relationships, and properties.
    \item[\texttt{SET}] Sets property values.
    \item[\texttt{FOREACH}] Performs an updating action for each element in a list.
    \item[\texttt{UNION}] Merges results from two or more queries.
    \item[\texttt{WITH}] Chains subsequent query parts and forwards results from one to the next. Similar to piping commands in Unix.
\end{itemize}

\subsubsection{Relational vs Graph Modeling}

\paragraph{tldr;} Turn entities into node, and relations into edges. No magic... Its a graph...

\paragraph{}The book explains this with the example of a system management domain (i.e. a set of server, racks, VMs, apps, clients, etc. that communicate/are related in certain ways). In a relational database each individual type would have an individual table that contains all instances of that type. Further, depending on the relation, there are additional tables connecting multiple rows of one table with multiple tables of another table (e.g. which app uses which services). Alternatively, in a 1-to-N relationship, this would be handled in the N side of the relation.

In a graph database each individual element in this construct would be a node, i.e., one node for each app, one node for each service, etc.. The nodes are then connected to represent the relations, all metadata is stored in labels and properties.

\paragraph{Query} The advantage of graph databases becomes apparent when you frequently query certain patterns in the graph. For example to find faulty equipment after a user complaint the following query would return all distinct faulty elements that affect \texttt{User 3}.

\begin{code}
\begin{asciiListing}
MATCH (user:User)-[*1..5]-(asset:Asset)
WHERE user.name = 'User 3' AND asset.status = 'down'
RETURN DISTINCT asset
\end{asciiListing}
\end{code}

\paragraph{Common Modeling Pitfalls} It is easy to miss important information when going from a relational model to a graph model. In the book this is shown in the form of an email relationship that stores who emailed whom with CC and BCC relationships to detect fraudulent behaviour, but the actual content of the email was lost in the process.
Basically, you need to think about what you are doing... Shocker!

\paragraph{Avoid Anti-Patterns} In general, do not encode entities into relationships. Use relationships to encode the \emph{how} two entities are related, and the quality of the relationship. It is easy to confuse nouns (which map to entities) with verbs (which map to relations). For example, while we might say ``Alice \emph{emailed} Bob'', it actually means ``Alice \emph{send} this email \emph{to} Bob'', which makes it more apparent that the email is an entity itself and  \emph{send} and \emph{to} are the relationships. I email is not a entity, we would loose the content of the email as this doesn't really fit into a relationship property (as an email might be send to multiple people).

Graphs are naturally additive structures. Adding many entities might feel wrong, as one wants to preserve query time, but this is not a consideration one should make while designing the database. To quote the book: ``Graph databases maintain fast query times even when storing vast amounts of data. Learning to trust our graph database is important when learning to structure our graphs without denormalizing them.''

\paragraph{Evolving the Domain} Adding additional data to a graph database is sometimes a lot easier than in relational databases, as we can simple add new nodes and edges, without affecting the preexisting graph.

\subsubsection{Identifying Nodes and Relationships}

Loose guideline to translate a concept into a graph:
\begin{itemize}
    \item Common nouns become Labels: ``user'' and ``email'' become the labels \texttt{User} and \texttt{Email}.
    \item Verbs that take an object become relationship names: “sent” and “wrote,” for example, become \texttt{SENT} and \texttt{WROTE}.
    \item A proper noun—a person or company’s name, for example—refers to an instance of a thing, which we model as a node, using one or more properties to capture that thing’s attributes.
\end{itemize}

\subsection{Graph Database Internals}

This subsection takes a brief look at the underlying architecture of a graph database.

\paragraph{Native Graph Processing} A native graph database like \emph{Neo4j} stores the relationships index free. That means there is not one table that lists all relationships of a specific type, but rather, each node store its outgoing connections locally, which greatly speeds up graph traversal.

\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/week12/byte_pattern.png}
    \caption{Neo4j node and relationship store file record structure}
    \label{fig:graphdb:byte_pattern}
\end{figure}

\paragraph{Native Graph Storage} Neo4j stores the graph in seperate stores which are represented by files on disks. There exists a store for nodes, relationships, properties, and labels. Storing the data for example for nodes and properties separately makes the underlying model slightly different from the view exposed to the user, however this greatly increases traversal performance.

The node store, like many other stores in Neo4j, is a fixed size store, that is every node in the graph will be exactly 9 bytes in size (I assume there is a mistake in the book as Figure \ref{fig:graphdb:byte_pattern} shows 15 bytes). This greatly aids lookup speeds. In the case of node data the first byte is a usage flag to indicate whether Neo4j can reclaim those bytes for new nodes. The next four bytes indicate the ID of the first relationship connected to the node, followed by four bytes for the first property for the node. The next five bytes point to the label store for this node. The final byte is reserved for flags and future use.

The relationship store is similar, with fixed sized entries. The record contains the IDs for both nodes of the connection, a pointer to the relationship type, two pointers for each node in the the relationship to the next/previous relationship (the form a doubly linked list for each node), and some flag bytes.

Traversing the data is very efficient. Due to the fixed sized entries one can compute the location of the next element in constant time, instead of doing a global lookup in a table (in the case of non-native graph databases).

To further improve traversal speeds, Neo4j implements an \emph{LRU-K page cache}, where each store is divided into distinct regions which are then cached in an LRU fashion. This allows the caching of graphs, which exceed the size of main memory.

\paragraph{Kernel API} This is the lowest level API available in Neo4j. It allows the user to listen to all transaction flowing through the system and react to them. One use case would be to intercept the deletion of nodes and only mark them as logically deleted in case the user wants to roll back the database at a later time.

\paragraph{Core API} The core API is a imperative Java API. It is lazily evaluated, as the nodes are traversed only when the code demands the next node. It also allows for transaction management capability to ensure ACID.

\paragraph{Traversal Framework} In this framework the user defines some restrictions for a traversal in a declarative Java API. The traversal is then performed by Neo4j as specified.

\paragraph{Cipher} Cipher is the SQL like query language seen above with the definition of the graph patterns in ASCII-art.

\subsubsection{Nonfunctional Characteristics}

\paragraph{Transactions} Transactions in Neo4j are semantically identical to traditional database transactions. Writes occur within a transaction context, with write locks being taken on any nodes and relationships involved in the transaction. On successful completion of the transaction, changes are flushed to disk and the locks are released. If the transaction fails, the writes are discarded. In case multiple transactions try to modify the same part of the graph, the transactions will be serialized.

\paragraph{Recoverability} When recovering from an unclean shutdown Neo4j checks in the most recently active transaction log and replays any transactions it finds against the store. In a cluster setting this is called \emph{local recovery}. After the local recovery is done the node needs to ``catch up'' with the other nodes. For this purpose the node request any transactions that occurred during the fail from another node (typically the master).

\paragraph{Availability} Typically, multiple database instances are clustered for high availability, with one master instance relaying all transactions to the other instances. At any moment the master and some replicas will be up to date, with some replicas catching up (in the order of milliseconds behind). Reads are generally distributed across the replicas to scale read performance.

\paragraph{Scale} Instead of only focusing on transactions per second, Neo4j focuses on a combination of capacity (graph size), latency, and read \& write throughput.

Neo4j can handle graphs with tens of billions of nodes, relationships, and properties. Apparently it could handle the Facebook graph.

Latency is not such a big issue in graph databases as in traditional relational databases. Since data is queried via pointer chasing and pattern matching, the latency depends on the size of data being queried, not the size of the data set.

For throughput one can note that complex read/write operations typically access the same region, i.e. sub-graph, of the graph multiple times. Due to the way the graph is stored it is very efficient to traverse local sections of a graph via pointer chasing.

\paragraph{Clustering} To spread a graph across multiple machines there are a few options. One way would be to split the graph into logical partitions from the client point of view, i.e. maintain a set of smaller graphs that are relatively independent.

If this is not case it is necessary to split a single graph across multiple machines. For this synthetic keys are used that point to data on other nodes. These keys are then resolved by the database when traversed.