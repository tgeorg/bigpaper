% ========== ========== ========== ==========
%
% The Hadoop Distributed File System
%
% ========== ========== ========== ==========
\section{The Hadoop Distributed File System \small{- tgeorg}}

\begin{quote}
  Source: \url{https://doi.org/10.1109/MSST.2010.5496972}
\end{quote}

\subsection{INTRODUCTION AND RELATED WORK}
Two types of nodes:
\begin{itemize}
  \item \textbf{NameNode} stores metadata
  \item \textbf{DataNode} stores application data
\end{itemize}
All nodes are fully connected and communicate with each other using TCP-based protocols. There are no data protection mechanisms in within DataNodes, instead data replication between nodes is used for redundancy.

\subsection{ARCHITECTURE}

\paragraph{NameNode}
"The HDFS namespace is a hierarchy of files and directories."\\
Files and directories are represented by \textit{inodes} on the NameNode.\\
"An HDFS client wanting to read a file first contacts the NameNode for the locations of data blocks comprising the file and then reads block contents from the DataNode closest to the client."\\
Blocks are typically 128MB but are user selectable file-by-file.\\
For writing data, the client first connects to the NameNode to nominate which DataNodes to write the replicas to.

\begin{itemize}
  \item \textbf{Image:} inode data and list of blocks belonging to a file. Stored in RAM.
  \item \textbf{Checkpoint:} persistent record of an image.
  \item \textbf{Journal:} modification log of the image stored on the NameNode on disk.
\end{itemize}

\paragraph{DataNodes}
"Each block replica on a DataNode is represented by two files in the local host’s native file system. The first file contains the data itself and the second file is block’s metadata including checksums for the block data and the block’s generation stamp."

\begin{itemize}
  \item namespace ID
        \begin{itemize}
          \item Identifies the cluster. Nodes with different namespace ID cannot join the cluster.
          \item New nodes do not have one and receive it when joininig the cluster.
        \end{itemize}
  \item storage ID
        \begin{itemize}
          \item Identifies the DataNode
          \item Persists through reboots
        \end{itemize}
\end{itemize}

Heartbeats
\begin{itemize}
  \item every 3 seconds
  \item timeout if none received within 10 minutes $\rightarrow$ NameNode considers DataNode out of service / replicas as unavailable
  \item content of heartbeat (total storage capacity, fraction of storage in use, and the number of data transfers currently in progress.)
\end{itemize}

\paragraph{HDFS Client}
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{figures/hdfsNewFile.png}
  \caption{HDFS client creating a new file}
  \label{fig:hdfs:newfile}
\end{figure}
Abstract interface, for user it seems as they are using standard filesystem (directories, etc.); user does not see where data is stored
\begin{itemize}
  \item \textbf{Read:} Ask NameNode which datanote contains which (replicas of) files, then contact DataNode directly
  \item \textbf{Write:} Ask NameNode, which DataNodes should host the (replicas of the) files
\end{itemize}

HDFS provides API that exposes location of file blocks (unlike conventional FS)

Default replication factor of block is 3

\paragraph{Image and Journal}
\begin{itemize}
  \item \textbf{namespace image:} file system metadata
  \item \textbf{checkpoint file:} persistent record of namespace image
  \item \textbf{journal:} write-ahead commit log for changes to the file system. Missing/corrupt checkpoint or journal results in (partly) lost namespace information.
\end{itemize}
Transactions are batched to prevent/reduce bottle neck caused by synchronous flush-and-sync


\paragraph{CheckpointNode}
Alternative roles for NameNodes: \textit{CheckpointNode} and \textit{BackupNode}
The CheckpointNode periodically combines the existing checkpoint and journal to create a new checkpoint and an empty journal. It downloads current check-point and journal files from NameNode, merges them locally, and returns new checkpoint back to NameNode.

\paragraph{BackupNode}
Next to periodic checkpoints, maintains up-to-date image of file system namespace. Quasi read-only NameNode. It can perform all operations of NameNode as long as they are read-only.

"Use of a BackupNode provides the option of running the NameNode without persistent storage, delegating responsibility for the namespace state per-sisting to the BackupNode"

\paragraph{Upgrades, File System Snapshots}
\textit{Snapshot}: current state of file system saved persistently
Local Snapshot on DataNode is created by creating copy of storage directory (not files themselves) and hard linking existing blocks. As deletion are done via removing original hard links and writes are COW\footnote{Copy-On-Write}, old blocks can still be recovered via the snapshot.
Snapshot is coordinated over all nodes.

\subsection{FILE I/O-OPERATIONS AND REPLICA MANGEMENT}

\paragraph{File Read and Write}
Data is added by creating new file and writing to it. After closing file cannot be changed except for appends.\\
For writing the writer receives a lease (a lock essentially), lease is periodically renewed via heartbeat.\\
Lease has soft and hard limit if not renewed
\begin{itemize}
  \item \textbf{soft limit:} Another client can kick writer
  \item \textbf{hard limit:} DataNode revokes lease of writer
\end{itemize}
Reading is never blocked by lease

\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{figures/hdfsPipeline.png}
  \caption{Data pipeline during block construction}
  \label{fig:hdfs:pipeline}
\end{figure}


Integrity of blocks is verified via checksums. when block is requested, checksum is sent together with it. Client verifies checksum and notifies NameNode if they do not match.\\
Block replicas are read based on distance, i.e. closest replica of a block first, if that fails / is corrupt then second closest is selected, ... etc.\\
HDFS I/O is optimised for batch processing

\paragraph{Block Placement}

Default HDFS replica placement:\\
General distribution: no more than one replica per node and no more than two replicas per rack (provided sufficient racks)

First replica is placed close to the writer. To determine distance network bandwidth and rack location are used. Block placement policy is adjustable.\\
After selecting targets for replicas, they are organised in a pipeline.\\
Reads are performed from the closest node, that hosts the wanted replica, first.\\
Together this should reduce inter-node and inter-rack traffic.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{figures/hdfsTopology.png}
  \caption{Cluster topology example}
  \label{fig:hdfs:topology}
\end{figure}

\paragraph{Replication management}


\begin{itemize}
  \item \textbf{Over-replicated:} If a block becomes over-replicated the NameNode decides which copy to remove. NameNode tries to maintain number of racks over which block has been replicated and prefers removing it from DataNode with low free disk space.
  \item \textbf{Under-replicated:} Block gets put in replication priority queue. Less replicas $\rightarrow$ higher priority
        Replication follows previous rule ($\leq$ 1 per node, $\leq$ 2 per rack)
\end{itemize}
Too many replications in one rack:
Treat block as under-replicated $\rightarrow$ replicate on different rack $\rightarrow$ now block is over-replicated $\rightarrow$ rule for over-replication is applied, removing the block from the rack in which it violates the rule $\leq$ 2 per rack

\paragraph{Balancer}
Block placement does not take disk space utilisation into account. DataNode disk usage is balanced if it is close to the mean total disk usage of the cluster minus some threshold.\\
"\textit{balancer}" tool is responsible for ensuring balanced disk usage on all DataNodes and moves blocks around accordingly. Has to garantee that number total number of replicas and number of racks hosting replicas does not get reduced.\\
Optimised to reduce inter-rack copying as well as possibility of bandwidth limits.

\paragraph{Block Scanner}
"Each DataNode runs a block scanner that periodically scans its block replicas and verifies that stored checksums match the block  data."
"If a client reads a complete block and checksum verification succeeds, it informs the DataNode. The DataNode treats it as a verification of the replica."\\
Verification time for each block is stored in log.\\
If block scanner or client detects corrupt block, NameNode is informed which marks block as corrupt. Corrupt blocks are not immediately deleted, instead a good copy is replicated first. Therefore data (even if corrupt) is preserved as long as possible.

\paragraph{Decommissioning}
Cluster administrator specifies which nodes can and cannot register with the cluster.\\
Cluster admin can command re-evaluation of include/exclude list. If DataNode is newly excluded, it gets marked for decommissioning. DataNode continues to serve read requests but no new data is added and existing blocks are replicated on other DataNodes. Once all blocks are replicated, the DataNode is markes as decommissioned.

\paragraph{Inter-Cluster Data Copy}
Done via a tool called DistCp. Uses MapReduce.

\subsection{PRACTICE AT "YAHOO!"}

(\textit{skipped, not relevant})

\paragraph{Durability of Data}
Three time replication is robust guard against data loss caused by uncorrelated node failures.\\
No guaranteed revocery against correlated node failures, e.g. whole rack falling out due to losing a rack switch, power loss, etc.\\
Block scanner regularly scans for corrupted data.

\paragraph{Caring for the Commons}
(\textit{skipped, not relevant})

\paragraph{Benchmarks}
(\textit{skipped, not relevant})

\subsection{FUTURE WORK}
Improve BackupNode ability to take over if NameNode is down.\\
Improve scalability of NameNode
$\rightarrow$ introduce multiple NameNode with independent namespaces.
