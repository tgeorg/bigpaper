% ========== ========== ========== ==========
%
%  XML in a Nutshell - The return of the XML
%
% ========== ========== ========== ==========
\section{XML in a Nutshell (Chapter 17 except 17.3) \small{- tgeorg}}

\begin{quote}
    Source: \url{https://proquest.safaribooksonline.com/0596007647}
\end{quote}

\subsection{Overview}

\textit{An XML Schema is an XML document containing a formal description of what comprises a valid XML document.}

\begin{itemize}
    \item An xsi:schemaLocation attribute on an element contains a list of namespaces used within that element and the URLs of the schemas with which to validate elements and attributes in those namespaces.
    \item An xsi:noNamespaceSchemaLocation attribute contains a URL for the schema used to validate elements that are not in any namespace.
    \item A validating parser is used to validate an XML document against a schema.
\end{itemize}

\subsection{Schema Basics}

Example:
The schema
\begin{lstlisting}
<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="fullName" type="xs:string"/>
</xs:schema>
\end{lstlisting}

validates
\begin{lstlisting}
<?xml version="1.0"?>
<fullName>Scott Means</fullName>
\end{lstlisting}

Note how the XML doc contains an element \texttt{fullName} which is referenced in the schema by \texttt{<xs:element name="fullName" type="xs:string"/>} with the name attribute in the schema matching the element in the XML.

\texttt{xsi:noNamespaceSchemaLocation} is used to tell the location of a schema without namespace, i.e.:

\begin{lstlisting}
<?xml version="1.0"?>
<fullName xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="address-schema.xsd">
    Scott Means
</fullName>
\end{lstlisting}

\paragraph{Document Organization}

"Every schema document consists of a single root \texttt{xs:schema} element. This element contains declarations for all elements and attributes that may appear in a valid
instance document." \\
"Instance elements declared using top-level xs:element elements in the schema (immediate child elements of the \texttt{xs:schema} element) are considered global elements. The simple schema in the example above globally declares one element: \texttt{fullName}. According to the rules of schema construction, any element that is declared globally may appear as the root element of an instance document."

\paragraph{Annotations}
XML comments not used for annotations as they might be lost during parsing. Instead "To accommodate this extra information, most schema elements may contain an optional xs:annotation element as their first child element. The annotation element may then, in turn, contain any combination of \texttt{xs:documentation} and \texttt{xs:appinfo} elements, which are provided to contain extra human-readable and machine-readable information, respectively."

Example:
%\begin{lstlisting}[language=XML, showstringspaces=false]
\begin{lstlisting}
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
        <xs:documentation xml:lang="en-US">
            Simple schema example from O'Reilly's
            <a href="http://www.oreilly.com/catalog xmlnut">XML in a Nutshell.</a>
            Copyright 2004 O'Reilly Media, Inc.
        </xs:documentation>
    </xs:annotation>
    <xs:element name="fullName" type="xs:string" />
</xs:schema>
\end{lstlisting}

"The \texttt{xs:documentation} element permits an xml:lang attribute to identify the language of the brief message. This attribute can also be applied to the \texttt{xs:schema} element to set the default language for the entire document."

\texttt{xs:documentation} used for human-readable content and \texttt{xs:appinfo} for application-specific extension information, e.g. to generate tool-tips in a GUI.

\paragraph{Element Declarations}
See figure \ref{fig:xml2:types}
\begin{figure}[h!] % TODO maybe replace with LaTeX table
    \centering
    \includegraphics[width=\textwidth]{figures/xml2SchemaTypes.png}
    \caption{Built-in XML schema types}
    \label{fig:xml2:types}
\end{figure}

\paragraph{Attribute Declarations}
Attributes are referenced in the schema using the element \texttt{xs:attribute}, so \texttt{<xs:attribute name="fruit" type="xs:string"/>} defines an attribute \texttt{fruit} of type string.

\subsection{Working with Namespaces}
\paragraph{Target Namespaces}
Use attribute \texttt{targetNamespace} to associate schema with namespace.
\begin{lstlisting}
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
  targetNamespace="http://namespaces.oreilly.com/xmlnut/address">
\end{lstlisting}

Namespace of schema and content of XML doc must match. E.g. if schema requires some element \texttt{A} and that element in some XML doc is not in the same namespace as the schema, then the schemal will detect it as missing.

\subsection{Complex Types}
While simple types are basic building blocks (integers, strings, etc.) complex types are a combination of different types, kinda like objects in object-oriented programming.
"Only elements can contain complex types. Attributes always have simple types."

XML:
\begin{lstlisting}
<?xml version="1.0"?>
<addr:address xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
 xsi:schemaLocation="http://namespaces.oreilly.com/xmlnut/address address-schema.xsd"
 xmlns:addr="http://namespaces.oreilly.com/xmlnut/address" addr:language="en"
>
  <addr:fullName>
      <addr:first>Scott</addr:first>
      <addr:last>Means</addr:last>
  </addr:fullName>
</addr:address>
\end{lstlisting}

Resulting schema:
\begin{lstlisting}
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" 
 targetNamespace="http://namespaces.oreilly.com/xmlnut/address" 
 xmlns:addr="http://namespaces.oreilly.com/xmlnut/address" 
 elementFormDefault="qualified"
>
    <xs:element name="address">
      <xs:complexType>
        <xs:sequence>
          <xs:element name="fullName">
            <xs:complexType>
              <xs:sequence>
                <xs:element name="first" type="addr:nameComponent" />
                <xs:element name="last" type="addr:nameComponent" />
              </xs:sequence>
            </xs:complexType>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
    </xs:element>
    <xs:complexType name="nameComponent">
      <xs:simpleContent>
        <xs:extension base="xs:string" />
      </xs:simpleContent>
    </xs:complexType>
  </xs:schema>
\end{lstlisting}
"The nested element declaration is for the fullName element, which then repeats the xs:complexType and xs:sequence definition process. Within this nested sequence, two element declarations appear for the first and last elements."
\paragraph{Occurence Constraints}
Use attributes \texttt{minOccurs} and \texttt{maxOccurs} to set min/max number of occurences.
\begin{lstlisting}
<xs:element name="fullName">
  <xs:complexType>
    <xs:sequence>
    <xs:element name="first" type="addr:nameComponent" />
    <xs:element name="middle" type="addr:nameComponent" minOccurs="0" />
    <xs:element name="last" type="addr:nameComponent" />
    </xs:sequence>
  </xs:complexType>
</xs:element>
\end{lstlisting}

\subsection{Empty Elements}
element has no content. All data is stored in attributes. Use complex type that only defines the attribute but no further elements $\rightarrow$ element must be empty to conform schema.

\subsection{Simple Content}
"The \texttt{xs:simpleType} element can define new simple data types, which can be referenced by element and attribute declarations within the schema."
\paragraph{Defining New Simple Types}
Example:
\begin{lstlisting}
<xs:complexType name="contactsType">
  <xs:sequence>
    <xs:element name="phone" minOccurs="0">
      <xs:complexType>
        <xs:attribute name="number" type="xs:string" />
        <xs:attribute name="location" type="addr:locationType" />
      </xs:complexType>
    </xs:element>
  </xs:sequence>
</xs:complexType>

<xs:simpleType name="locationType">
  <xs:restriction base="xs:string" />
</xs:simpleType>
\end{lstlisting}
\paragraph{Facets}
"A facet is an aspect of a possible value for a simple data type."
Facet types that are supported by a schema processor:
\begin{itemize}
    \item \texttt{length} (or minLength and maxLength) (restricts length of string)
    \item \texttt{pattern} (to enforce a specific format, e.g. \texttt{"\textbackslash d\textbackslash d-\textbackslash d\textbackslash d\textbackslash d-\textbackslash d\textbackslash d\textbackslash d"} for Legi number)
    \item \texttt{enumeration} (only allows predefined valid options)
    \item \texttt{whiteSpace} (preserve, replace, collapse)
    \item \texttt{maxInclusive}, \texttt{maxExclusive}, \texttt{minInclusive}, and \texttt{minExclusive} (to restrict range of numeric values)
    \item \texttt{totalDigits} (total number of digits allowed in number)
    \item \texttt{fractionDigits} (\texttt{totalDigits} after the decimal point)
\end{itemize}
Facets are applied to simple types using the \texttt{xs:restriction} element.
Use lists to allow multiple of same type (e.g. multiple middle names) and unions to allow multiple types (e.g. accept both strings and integers)
\subsection{Mixed Content}
\paragraph{Allowing Mixed Content}
By setting the attribute \texttt{mixed="true"} of a complex type we say that the elements contained in this complexType can be of different types.

\paragraph{Controlling Element Placement}
\begin{itemize}
    \item \texttt{xs:sequence} for a sequence of elements listed in the same order as the schema
    \item \texttt{xs:all} same as \texttt{xs:sequence} but without the order restriction
    \item \texttt{xs:choice} for enumeration, i.e. only values allowed are those listed within
\end{itemize}

\paragraph{Using Groups}
\texttt{xs:group} can be used to define a structure of elements and reference it to reuse it. Kinda like functions are used in programs to reuse code.

\subsection{Allowing Any Content}
Use \texttt{xs:any} if you don't care about the content type.

\paragraph{Using Multiple Documents}
Use \texttt{xs:include} with attribute \texttt{schemaLocation} to "import" other schema files and re-use their content.

If you want to adjust the imported schema, use the element \texttt{xs:redefine} instead of \texttt{xs:include}. To import the schema under a different namespace, use \texttt{xs:import} with attributes \texttt{schemaLocation} and \texttt{namespace}.

\paragraph{Derived Complex Types}
The element \texttt{xs:extension} can be used to extend parts of the imported schema similar to object-oriented programming where you can create a subclass of and object and extend it.

\subsection{Controlling Type Derivation}
Similar to object-oriented programming "the schema language allows schema authors to place restrictions on type extension and restriction."
\paragraph{Abstract Elements and Types}
"The abstract attribute applies to type and element declarations. When it is set to true, that element or type cannot appear directly in an instance document."
\paragraph{The Final Attribute}
The attribute \texttt{final} allows to prevent extensions of an element in the schema.

\paragraph{Uniqueness and Keys}
"The \texttt{xs:unique} element enforces element and attribute value uniqueness for a specified set of elements in a schema document."
I.e. no same value for element and attributes where uniqueness is enforced.\\
\texttt{xs:key} also enforces uniqueness but unlike \texttt{xs:unique} gives an error if no value exists.